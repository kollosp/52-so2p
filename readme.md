### Uruchomienie

* kompilacja w katalogu `Release` lub `Debug`
* uruchomienie aplikacji ./52-so2p


### Konfiguracja 

./52-so2p 1 2 3 4
 1. liczba filozofów
 2. czas jedzenia
 3. czas myślenia przed rozpoczęciem oczekiwania na jedzenie
 4. czas życia (od skończenia jedzenia do śmierci)
